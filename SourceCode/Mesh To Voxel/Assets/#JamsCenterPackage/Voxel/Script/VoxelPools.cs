﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VoxelPools : MonoBehaviour {

    public VoxelPool[] m_pools;
    public int m_currentIndex;
    public int m_currentLocalIndex;
    public VoxelPool m_selectedPool;
    public int m_totalVoxel;



    internal int GetVoxelsCount()
    {
        int voxel = 0;
        for (int i = 0; i < m_pools.Length; i++)
        {
            voxel += m_pools[i].GetVoxelsCount();
        }
        return voxel;
    }

    int poolIndex;
    VoxelPool pool;
    internal void SetVoxel(int i, Color color)
    {
        ConvertIndexToLocalPool(i, out poolIndex, out pool);
        pool.SetVoxel(poolIndex, color);
    }



    int localindex;
    VoxelPool pooll;
    internal Transform GetVoxelTransform(int i)
    {
        ConvertIndexToLocalPool(i, out localindex, out pooll);
        m_currentLocalIndex = localindex;
        m_currentIndex ++;
        m_currentIndex %= GetVoxelsCount();
        m_selectedPool = pooll;
        return pooll.GetVoxelTransform(localindex);
    }

    int toRemove ;
    int selectedPool;
    int poolSize;
    bool poolFouned ;
    int totalIndex ;

    private void ConvertIndexToLocalPool(int index, out int poolIndex, out VoxelPool pool)
    {
         toRemove =0;
         selectedPool = 0;
         poolSize = 0;

        pool = null;
        poolIndex = 0;

         poolFouned = false;
         totalIndex = GetVoxelsCount();

        index %= totalIndex;

        if (m_pools.Length == 1)
        {
            poolIndex = index;
            pool = m_pools[0];
            return;
        }

        do {

            poolSize +=m_pools[selectedPool].GetVoxelsCount();


            if (index < poolSize) {
                
                pool = m_pools [selectedPool];
                poolFouned = true;
                poolIndex = index - toRemove;   
            }
            toRemove = poolSize;
            selectedPool++;

        }
        while (!poolFouned);
        
    }

    public void OnValidate()
    {
        m_totalVoxel = GetVoxelsCount();
    }
}
