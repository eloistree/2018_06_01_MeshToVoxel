﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(BoxScanToVoxel))]
public class BoxScanToVoxelEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        BoxScanToVoxel myScript = (BoxScanToVoxel)target;
        if (Application.isPlaying &&  GUILayout.Button("Generate"))
        {
            myScript.StartScanning();
        }
        if (myScript.IsScanning())
        {
            EditorGUILayout.HelpBox(string.Format("Is Scanning... {0:00.}%", myScript.m_poucentScanning*100f), MessageType.Warning);
        }
    }
}

