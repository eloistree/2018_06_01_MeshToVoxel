﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VoxelPool : MonoBehaviour {


    public Renderer m_renderer;
    public Texture2D m_texture;
    public Transform [] m_voxels;



    public void Awake()
    {
        Texture2D t = new Texture2D(m_texture.width, m_texture.height);
        t.filterMode = FilterMode.Point;
        t.SetPixels(m_texture.GetPixels());
        m_texture = t;
        m_renderer.material = new Material(m_renderer.material);
        m_renderer.material.mainTexture = t;
       
    }

    public void Update()
    {

        m_texture.Apply();
    }

    public void SetVoxel(int x, int y , Color color)
    {
        m_texture.SetPixel(x, y, color);
        //m_texture.Apply();
    }

    internal Transform GetVoxelTransform(int index)
    {
        return m_voxels[index];
    }

    public void SetVoxel(int index, Color color)
    {
        int x, y;
        ConvertToXY(index, out x, out y);
        SetVoxel(x, y, color);
    }

    public Color GetColor(int x, int y)
    {

        int index;
        ConvertToIndex(x, y,out index);
        return GetColor(index);
    }

    public Color GetColor(int index) {

      return  m_texture.GetPixels()[index];
    }

    public void ConvertToXY(int index, out int x, out int y)
    {
       x = index % m_texture.width;
       y = (int)index / m_texture.height;

    }
    public void ConvertToIndex(int x, int y, out int index)
    {
        index = y * m_texture.width + x;

    }

    public int GetVoxelsCount() { return m_voxels.Length; }
}

