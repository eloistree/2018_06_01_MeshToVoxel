﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class TriggerOnCameraDirection : MonoBehaviour {


    [Header("Params")]
    public Transform m_objectTracked;
    public Transform m_cameraTracked;
    public float m_angle;

    public Direction m_selectedDirection;
    public Vector3 m_direction;

    [Header("Event")]

    public OnStateChangeEvent m_conditionStateChange;

    [Header("Debug")]
    public bool isTrueCondition;
    public float currentAngle;

    [System.Serializable]
    public class OnStateChangeEvent : UnityEvent<bool> { }


    public enum Direction
    {
        custom,
        up,
        down,
        right,
        left,
        front,
        back
    }


    //public Transform d_camera;
    public Transform d_object;

	// Use this for initialization
	void Start () {



		
	}


    // Update is called once per frame
    void Update() {

        bool oldCondition = isTrueCondition;
        DebugDraw.duration = Time.deltaTime;
        DebugDraw.Cartesian(Vector3.zero + Vector3.one * 5, Quaternion.identity, 5, Space.World);

        //d_camera.position = Vector3.zero + Vector3.one * 5;
        ///d_camera.rotation = Quaternion.identity;

        Vector3 localPositionOfObject = m_cameraTracked.InverseTransformPoint(m_objectTracked.position);
        Vector3 objectDirection = m_cameraTracked.InverseTransformDirection(m_objectTracked.up);
        Quaternion directionFromCamera = Quaternion.Inverse(m_cameraTracked.rotation) * m_objectTracked.rotation;

        DebugDraw.Cartesian(localPositionOfObject + Vector3.one * 5, directionFromCamera, 3, Space.World);

        if (d_object != null) {


            d_object.position = localPositionOfObject + Vector3.one * 5;
            d_object.rotation = directionFromCamera;
        }

        currentAngle = Vector3.Angle(m_direction, objectDirection);
        isTrueCondition = currentAngle <= m_angle;

        if (oldCondition != isTrueCondition)
        {
            m_conditionStateChange.Invoke(isTrueCondition);
        }
        
    }

    public void OnValidate()
    {

        switch (m_selectedDirection)
        {
            case Direction.up:
                m_direction = Vector3.up;
                break;
            case Direction.down:
                m_direction = Vector3.down;
                break;
            case Direction.right:
                m_direction = Vector3.right;
                break;
            case Direction.left:
                m_direction = Vector3.left;
                break;
            case Direction.front:
                m_direction = Vector3.forward;
                break;
            case Direction.back:
                m_direction = Vector3.back;
                break;
            default:
                break;
        }
    }
}
